package com.example.user.applicationforschool

import android.content.ContentValues
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import android.content.Intent
import android.database.Cursor
import android.graphics.Color
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import java.util.*


class ClassInfoActivity : AppCompatActivity() {

    lateinit var txt:TextView
    lateinit var txt2:TextView
    val dbHelper= DBHelper(this)
    lateinit var c : Cursor
    var colors = IntArray(3)
    val idBut : Int = 1;
    val MENU_DEL:Int=1
    val MENU_UPD:Int=2
    var BTN_TXT:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_class_info)

        txt=findViewById(R.id.textView) as TextView
        txt2=findViewById(R.id.textView2) as TextView
        txt.text = intent.getStringExtra("numClass")
        txt2.text=readClass().toString()

        colors[0] = Color.parseColor("#559966CC")
        colors[1] = Color.parseColor("#55336699")
        colors[2] = Color.parseColor("#00008b")
        val linLayout = findViewById(R.id.linLayout) as LinearLayout
        val ltInflater = layoutInflater

        val item2 = ltInflater.inflate(R.layout.item, linLayout, false)
        val but2=item2.findViewById(R.id.button1) as Button
        but2.text="<--------"
        but2.id=idBut+1
        but2.setOnClickListener { finish(); val intent = Intent(this, MainActivity::class.java);startActivity(intent); }
        item2.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        linLayout.addView(item2)

        val item1 = ltInflater.inflate(R.layout.item, linLayout, false)
        val but1=item1.findViewById(R.id.button1) as Button
        but1.text="Добавить ребёнка"
        but1.id=idBut
        but1.setOnClickListener { addButClick() }
        item1.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        linLayout.addView(item1)


        val list: MutableList<String> = readChildrens()
        if(list[0]!="NULL") {
            for (i in 0..list.size-1 ) {
                val item = ltInflater.inflate(R.layout.item, linLayout, false)
                val but = item.findViewById(R.id.button1) as Button
                but.text = list[i]
                but.setOnClickListener {
                    studClick(list[i])
                }
                registerForContextMenu(but)
                item.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
                item.setBackgroundColor(colors[i % 2])
                linLayout.addView(item)
            }
        }
    }

    fun readClass():Int{
        var rezult:Int=0
        val db = dbHelper.readableDatabase
        c = db.query("classesTable", null, null, null, null, null, null)
        if (c.moveToFirst()) {
            val numOfClass = c.getColumnIndex("numOfClass")
            val numOfChildrensInClass = c.getColumnIndex("numOfChildrensInClass")
            do {
                if(c.getString(numOfClass)==txt.text.toString()) {
                    rezult = c.getInt(numOfChildrensInClass)
                    break
                }
            } while (c.moveToNext())
        }
        else rezult=0
        c.close()
        dbHelper.close()
        return rezult
    }

    fun readChildrens():MutableList<String>{
        val list:MutableList<String> = ArrayList()
        val db = dbHelper.readableDatabase
        c = db.query("childrensTable", null, null, null, null, null, null)
        if (c.moveToFirst()) {
            val name = c.getColumnIndex("Name")
            val numOfClass = c.getColumnIndex("numOfClass")
            do {
                if(c.getString(numOfClass)==txt.text) {
                    list.add(c.getString(name))
                }
            } while (c.moveToNext())
        }
        else list.add("NULL")
        if(list.isEmpty()) list.add("NULL")
        c.close()
        dbHelper.close()
        list.sort()
        return list
    }

    fun addButClick(){
        finish()
        val intent = Intent(this, AddChildActivity::class.java)
        intent.putExtra("numClass",txt.text.toString())
        startActivity(intent)
    }

    fun studClick(name: String){
        finish()
        val intent = Intent(this, ChildInfoActivity::class.java)
        intent.putExtra("name",name)
        intent.putExtra("classNum",txt.text)
        startActivity(intent)
    }

    public override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        menu?.add(0, MENU_DEL, 0, "Удалить");
        menu?.add(0, MENU_UPD, 0, "Изменить");
        val bt=v as Button
        BTN_TXT= bt.text.toString()
    }

    public override fun onContextItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            MENU_DEL -> {
                deleteChildFromClass(BTN_TXT, txt.text.toString())
            }
            MENU_UPD ->{
                updateChild(BTN_TXT, txt.text.toString())
            }
        }

        return super.onContextItemSelected(item)
    }

    fun deleteChildFromClass(name:String, numClass:String){
        val db = dbHelper.writableDatabase
        val args = arrayOf<String>(numClass,name)
        db.delete("childrensTable"," numOfClass = ? AND Name = ? ",args)
        dbHelper.close()
        updateClass()
        backToClass(numClass)
    }

    fun updateChild(name:String, numClass:String){
        val list: MutableList<String> = readChild(numClass,name)

        finish()
        val intent = Intent(this, UpdateChildActivity::class.java)
        intent.putExtra("classNum",numClass)
        intent.putExtra("Name",name)
        intent.putExtra("birthday",list[1])
        intent.putExtra("telephone",list[2])
        intent.putExtra("motherName",list[4])
        intent.putExtra("motherTelephone",list[5])
        intent.putExtra("fatherName",list[6])
        intent.putExtra("fatherTelephone",list[7])
        intent.putExtra("address",list[3])
        startActivity(intent)
    }

    fun backToClass(numClass :String){
        finish()
        val intent = Intent(this, ClassInfoActivity::class.java)
        intent.putExtra("numClass",numClass)
        startActivity(intent)
    }

    fun readChild(numClass: String, nameChild: String):MutableList<String>{
        val list:MutableList<String> = ArrayList()
        val db = dbHelper.readableDatabase
        c = db.query("childrensTable", null, null, null, null, null, null)
        if (c.moveToFirst()) {
            val name = c.getColumnIndex("Name")
            val birthday = c.getColumnIndex("BirthDay")
            val numofclass = c.getColumnIndex("numOfClass")
            val telephone = c.getColumnIndex("TelephoneNumber")
            val address = c.getColumnIndex("Address")
            val mothername = c.getColumnIndex("motherName")
            val mothertelephone = c.getColumnIndex("motherTelephoneNumber")
            val fathername = c.getColumnIndex("fatherName")
            val fathertelephone = c.getColumnIndex("fatherTelephoneNumber")

            do {
                if(c.getString(name)==nameChild && c.getString(numofclass)==numClass) {
                    list.add(c.getString(name))
                    list.add(c.getString(birthday))
                    list.add(c.getString(telephone))
                    list.add(c.getString(address))
                    list.add(c.getString(mothername))
                    list.add(c.getString(mothertelephone))
                    list.add(c.getString(fathername))
                    list.add(c.getString(fathertelephone))
                    break
                }
            } while (c.moveToNext())
        }
        else list.add("NULL")
        c.close()
        dbHelper.close()
        return list
    }

    fun updateClass(){
        val db = dbHelper.readableDatabase
        c = db.query("classesTable", null, null, null, null, null, null)
        if (c.moveToFirst()) {
            val numOfClass = c.getColumnIndex("numOfClass")
            val numOfChildrensInClass = c.getColumnIndex("numOfChildrensInClass")
            do {
                if(c.getString(numOfClass)==txt.text.toString()) {
                    updateTab(c.getInt(numOfChildrensInClass))
                    break
                }
            } while (c.moveToNext())
        }
        c.close()
        dbHelper.close()
    }

    fun updateTab(numOfChild:Int){
        val db = dbHelper.writableDatabase
        val cv = ContentValues()
        cv.put("numOfChildrensInClass",numOfChild-1 )
        val args = arrayOf<String>(txt.text.toString())
        db.update("classesTable", cv, "numOfClass = ?", args)
    }

}