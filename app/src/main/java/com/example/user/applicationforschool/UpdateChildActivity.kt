package com.example.user.applicationforschool

import android.content.ContentValues
import android.content.Intent
import android.database.Cursor
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class UpdateChildActivity : AppCompatActivity() {

    lateinit var name: EditText
    lateinit var birthday: EditText
    lateinit var telephone: EditText
    lateinit var motherName: EditText
    lateinit var motherTelephone: EditText
    lateinit var fatherName: EditText
    lateinit var fatherTelephone: EditText
    lateinit var address: EditText
    val dbHelper= DBHelper(this)
    lateinit var c : Cursor
    lateinit var curName : String
    lateinit var numClass : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_child)
        numClass=intent.getStringExtra("classNum")
        var exitBtn: Button =findViewById(R.id.buttonExit) as Button
        exitBtn.text="<-------"
        exitBtn.setOnClickListener { backToClass(numClass)}

        curName = intent.getStringExtra("Name")
        var curBirthday:String=intent.getStringExtra("birthday")
        var curTelephone:String=intent.getStringExtra("telephone")
        var curMotherName:String=intent.getStringExtra("motherName")
        var curMotherTelephone:String=intent.getStringExtra("motherTelephone")
        var curFatherName:String=intent.getStringExtra("fatherName")
        var curFatherTelephone:String=intent.getStringExtra("fatherTelephone")
        var curAddress:String=intent.getStringExtra("address")

        name = findViewById(R.id.name) as EditText;
        birthday = findViewById(R.id.birthday) as EditText;
        telephone = findViewById(R.id.telephone) as EditText;
        motherName = findViewById(R.id.motherName) as EditText;
        motherTelephone = findViewById(R.id.motherTelephone) as EditText;
        fatherName = findViewById(R.id.fatherName) as EditText;
        fatherTelephone = findViewById(R.id.fatherTelephone) as EditText;
        address = findViewById(R.id.address) as EditText;

        name.text.insert(0,curName)
        birthday.text.insert(0,curBirthday)
        telephone.text.insert(0, curTelephone)
        motherName.text.insert(0, curMotherName)
        motherTelephone.text.insert(0, curMotherTelephone)
        fatherName.text.insert(0, curFatherName)
        fatherTelephone.text.insert(0, curFatherTelephone)
        address.text.insert(0, curAddress)

        var saveBtn: Button =findViewById(R.id.buttonSave) as Button
        saveBtn.setOnClickListener {saveBtnClick() }
    }

    fun backToClass(numClass :String){
        finish()
        val intent = Intent(this, ClassInfoActivity::class.java)
        intent.putExtra("numClass",numClass)
        startActivity(intent)

    }

    fun saveBtnClick(){
        val db = dbHelper.writableDatabase
        val cv = ContentValues()
        cv.put("Name",name.text.toString() )
        cv.put("BirthDay",birthday.text.toString() )
        cv.put("TelephoneNumber",telephone.text.toString() )
        cv.put("motherName",motherName.text.toString() )
        cv.put("motherTelephoneNumber",motherTelephone.text.toString() )
        cv.put("fatherName",fatherName.text.toString() )
        cv.put("fatherTelephoneNumber",fatherTelephone.text.toString() )
        cv.put("Address",address.text.toString() )
        val args = arrayOf<String>(curName,numClass)
        db.update("childrensTable", cv, " Name = ? AND numOfClass = ? ", args)
        backToClass(numClass)
    }
}