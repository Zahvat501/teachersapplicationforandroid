package com.example.user.applicationforschool

import android.content.Context
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.ContentValues.TAG
import android.util.Log


/**
 * Created by User on 19.11.2016.
 */

class DBHelper(context: Context)
    : SQLiteOpenHelper(context, "myDataBase4", null, 1)
{
    override fun onCreate(db: SQLiteDatabase?) {
        if(db!=null) {
            db.execSQL(" CREATE TABLE IF NOT EXISTS classesTable ("
                    + " numOfClass string ,"
                    + " numOfChildrensInClass integer "
                    + ");")
            db.execSQL(" CREATE TABLE IF NOT EXISTS childrensTable ("
                    + " Name string, "
                    + " BirthDay string, "
                    + " numOfClass string, "
                    + " TelephoneNumber string, "
                    + " motherName string, "
                    + " motherTelephoneNumber string, "
                    + " fatherName string, "
                    + " fatherTelephoneNumber string, "
                    + " Address string "
                    + ");")

            Log.i(TAG, "Database create classesTable");
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}