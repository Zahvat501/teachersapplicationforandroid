package com.example.user.applicationforschool

import android.app.Activity
import android.os.Bundle
import android.content.ContentValues;
import android.database.Cursor;
import java.util.*
import android.content.Intent
import android.widget.*


/**
 * Created by User on 22.11.2016.
 */

class AddClassActivity internal constructor() : Activity() {

    val dbHelper= DBHelper(this)
    lateinit var c : Cursor

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_class)

        var exitBtn:Button=findViewById(R.id.buttonExit) as Button
        exitBtn.text="<-------"
        exitBtn.setOnClickListener { finish(); val intent = Intent(this, MainActivity::class.java);startActivity(intent);}

        var btn = findViewById(R.id.buttonAddClass) as Button
        btn.setOnClickListener {
            addClassBtn()
        }
    }

    fun addClass(numOfClass: String, numOfChildrensInClass: Int){
        val cv = ContentValues()
        val db = dbHelper.writableDatabase
        cv.put("numOfClass", numOfClass)
        cv.put("numOfChildrensInClass", numOfChildrensInClass)
        val rowID = db.insert("classesTable", null, cv)
        dbHelper.close()
    }

    fun readClass():MutableList<String>{
        val list:MutableList<String> = ArrayList()
        val db = dbHelper.readableDatabase
        c = db.query("classesTable", null, null, null, null, null, null)
        if (c.moveToFirst()) {
            val numOfClass = c.getColumnIndex("numOfClass")
            do {
                list.add(c.getString(numOfClass))
            } while (c.moveToNext())
        }
        else list.add("Вы еще не внесли в список не одного класса.")
        c.close()
        dbHelper.close()
        return list
    }

    fun delClass(numOfClass: String) {
        val db = dbHelper.writableDatabase
        val clearCount = db.delete("classesTable", "numOfClass"+numOfClass, null)
        dbHelper.close()
    }

    fun addClassBtn() {
        val ClassNumText:EditText = findViewById(R.id.NumCl) as EditText;

        if(ClassNumText.text.toString()!="" && ClassNumText.text.toString()!="Введите номер класса") {
            addClass(ClassNumText.text.toString(), 0)
            finish()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}