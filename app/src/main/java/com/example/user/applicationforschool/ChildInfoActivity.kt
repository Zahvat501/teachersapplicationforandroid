package com.example.user.applicationforschool

import android.content.Intent
import android.database.Cursor
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import java.util.*

class ChildInfoActivity : AppCompatActivity() {

    val dbHelper= DBHelper(this)
    lateinit var c : Cursor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_child_info)

        var exitBtn: Button =findViewById(R.id.buttonExit) as Button
        exitBtn.text="<-------"
        exitBtn.setOnClickListener { backToClass(intent.getStringExtra("classNum"))}

        var name:TextView=findViewById(R.id.Name) as TextView
        var birthday:TextView=findViewById(R.id.birthday) as TextView
        var telephone:TextView=findViewById(R.id.telephone) as TextView
        var address:TextView=findViewById(R.id.address) as TextView
        var motherName:TextView=findViewById(R.id.motherName) as TextView
        var motherTelephone:TextView=findViewById(R.id.motherTelephote) as TextView
        var fatherName:TextView=findViewById(R.id.fatherName) as TextView
        var fatherTelephone:TextView=findViewById(R.id.fatherTelephone) as TextView

        val list: MutableList<String> = readChild(intent.getStringExtra("classNum"),intent.getStringExtra("name"))
        if(list[0]!="NULL") {
            var s: String = name.text.toString()+ " " + list[0]
            name.text=s

            s = birthday.text.toString()+ " " + list[1]
            birthday.text=s

            s = telephone.text.toString()+ " " + list[2]
            telephone.text=s

            s = address.text.toString()+ " " + list[3]
            address.text=s

            s = motherName.text.toString()+ " " + list[4]
            motherName.text=s

            s = motherTelephone.text.toString()+ " " + list[5]
            motherTelephone.text=s

            s = fatherName.text.toString()+ " " + list[6]
            fatherName.text=s

            s = fatherTelephone.text.toString()+ " " + list[7]
            fatherTelephone.text=s
        }
    }

    fun readChild(numClass: String, nameChild: String):MutableList<String>{
        val list:MutableList<String> = ArrayList()
        val db = dbHelper.readableDatabase
        c = db.query("childrensTable", null, null, null, null, null, null)
        if (c.moveToFirst()) {
            val name = c.getColumnIndex("Name")
            val birthday = c.getColumnIndex("BirthDay")
            val numofclass = c.getColumnIndex("numOfClass")
            val telephone = c.getColumnIndex("TelephoneNumber")
            val address = c.getColumnIndex("Address")
            val mothername = c.getColumnIndex("motherName")
            val mothertelephone = c.getColumnIndex("motherTelephoneNumber")
            val fathername = c.getColumnIndex("fatherName")
            val fathertelephone = c.getColumnIndex("fatherTelephoneNumber")

            do {
                if(c.getString(name)==nameChild && c.getString(numofclass)==numClass) {
                    list.add(c.getString(name))
                    list.add(c.getString(birthday))
                    list.add(c.getString(telephone))
                    list.add(c.getString(address))
                    list.add(c.getString(mothername))
                    list.add(c.getString(mothertelephone))
                    list.add(c.getString(fathername))
                    list.add(c.getString(fathertelephone))
                    break
                }
            } while (c.moveToNext())
        }
        else list.add("NULL")
        c.close()
        dbHelper.close()
        return list
    }

    fun backToClass(numClass :String){
        finish()
        val intent = Intent(this, ClassInfoActivity::class.java)
        intent.putExtra("numClass",numClass)
        startActivity(intent)
    }
}
