package com.example.user.applicationforschool

import android.content.ContentValues
import android.content.Intent
import android.database.Cursor
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class AddChildActivity : AppCompatActivity() {

    val dbHelper= DBHelper(this)
    lateinit var c : Cursor
    lateinit var numClass:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_child)

        numClass=intent.getStringExtra("numClass")

        var exitBtn:Button=findViewById(R.id.buttonExit) as Button
        exitBtn.text="<-------"
        exitBtn.setOnClickListener { backToClass(numClass)}

        var btn = findViewById(R.id.buttonAddChild) as Button
        btn.setOnClickListener {
            addChildBtn(numClass)
        }
    }

    fun addChildBtn(numClass:String){
        val name: EditText = findViewById(R.id.name) as EditText;
        val birthday:EditText = findViewById(R.id.birthday) as EditText;
        val telephone:EditText = findViewById(R.id.telephone) as EditText;
        val motherName:EditText = findViewById(R.id.motherName) as EditText;
        val motherTelephone:EditText = findViewById(R.id.motherTelephone) as EditText;
        val fatherName:EditText = findViewById(R.id.fatherName) as EditText;
        val fatherTelephone:EditText = findViewById(R.id.fatherTelephone) as EditText;
        val address:EditText = findViewById(R.id.address) as EditText;

        if(name.text.toString()!="" && name.text.toString()!="Ф.И.О. ребёнка" &&
                birthday.text.toString()!="" && birthday.text.toString()!="Дата рождения" &&
                telephone.text.toString()!="" && telephone.text.toString()!="Телефон" &&
                motherName.text.toString()!="" && motherName.text.toString()!="Ф.И.О. матери" &&
                motherTelephone.text.toString()!="" && motherTelephone.text.toString()!="Телефон матери" &&
                fatherName.text.toString()!="" && fatherName.text.toString()!="Ф.И.О. отца" &&
                fatherTelephone.text.toString()!="" && fatherTelephone.text.toString()!="Телефон отца" &&
                address.text.toString()!="" && address.text.toString()!="Адрес" ) {

            val cv = ContentValues()
            val db = dbHelper.writableDatabase
            cv.put("Name", name.text.toString())
            cv.put("Birthday", birthday.text.toString())
            cv.put("numOfClass", numClass)
            cv.put("TelephoneNumber", telephone.text.toString())
            cv.put("motherName", motherName.text.toString())
            cv.put("motherTelephoneNumber", motherTelephone.text.toString())
            cv.put("fatherName", fatherName.text.toString())
            cv.put("fatherTelephoneNumber", fatherTelephone.text.toString())
            cv.put("Address", address.text.toString())
            db.insert("childrensTable", null, cv)
            updateClass()
            dbHelper.close()
            backToClass(numClass)
        }
    }

    fun readClass():Int{
        var rezult:Int=0
        val db = dbHelper.readableDatabase
        c = db.query("classesTable", null, null, null, null, null, null)
        if (c.moveToFirst()) {
            val numOfClass = c.getColumnIndex("numOfClass")
            val numOfChildrensInClass = c.getColumnIndex("numOfChildrensInClass")
            do {
                if(c.getString(numOfClass)==numClass) {
                    rezult = c.getInt(numOfChildrensInClass)
                    break
                }
            } while (c.moveToNext())
        }
        else rezult=0
        c.close()
        dbHelper.close()
        return rezult
    }

    fun updateClass(){
        val db = dbHelper.readableDatabase
        c = db.query("classesTable", null, null, null, null, null, null)
        if (c.moveToFirst()) {
            val numOfClass = c.getColumnIndex("numOfClass")
            val numOfChildrensInClass = c.getColumnIndex("numOfChildrensInClass")
            do {
                if(c.getString(numOfClass)==numClass) {
                    updateTab(c.getInt(numOfChildrensInClass))
                    break
                }
            } while (c.moveToNext())
        }
        c.close()
        dbHelper.close()
    }

    fun updateTab(numOfChild:Int){
        val db = dbHelper.writableDatabase
        val cv = ContentValues()
        cv.put("numOfChildrensInClass",numOfChild+1 )
        val args = arrayOf<String>(numClass)
        db.update("classesTable", cv, "numOfClass = ?", args)
    }

    fun backToClass(numClass :String){
        finish()
        val intent = Intent(this, ClassInfoActivity::class.java)
        intent.putExtra("numClass",numClass)
        startActivity(intent)
    }
}
