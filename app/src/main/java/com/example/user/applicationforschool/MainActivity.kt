package com.example.user.applicationforschool

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup.LayoutParams
import android.widget.Button
import android.widget.LinearLayout
import android.database.Cursor;
import java.util.*
import android.content.Intent
import android.view.ContextMenu
import android.view.MenuItem

class MainActivity : AppCompatActivity() {
    var colors = IntArray(3)
    val dbHelper= DBHelper(this)
    lateinit var c : Cursor
    val idBut : Int = 1;
    val MENU_DEL:Int=1
    var BTN_TXT:String=""

    /** Called when the activity is first created.  */
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        colors[0] = Color.parseColor("#559966CC")
        colors[1] = Color.parseColor("#55336699")
        colors[2] = Color.parseColor("#00008b")
        val linLayout = findViewById(R.id.linLayout) as LinearLayout
        val ltInflater = layoutInflater
        val item1 = ltInflater.inflate(R.layout.item, linLayout, false)
        val but1=item1.findViewById(R.id.button1) as Button
        but1.text="Добавить класс"
        but1.id=idBut
        but1.setOnClickListener { addButClick() }
        item1.layoutParams.width = LayoutParams.MATCH_PARENT
        linLayout.addView(item1)
        val list: MutableList<String> = readClass()
        if(list[0]!="NULL") {
            for (i in 0..list.size - 1) {
                val item = ltInflater.inflate(R.layout.item, linLayout, false)
                val but = item.findViewById(R.id.button1) as Button
                but.text = list[i]
                but.setOnClickListener {
                    classClick(list[i])
                }
                registerForContextMenu(but)
                item.layoutParams.width = LayoutParams.MATCH_PARENT
                item.setBackgroundColor(colors[i % 2])
                linLayout.addView(item)
            }
        }
    }

    fun readClass():MutableList<String>{
        val list:MutableList<String> = ArrayList()
        val db = dbHelper.readableDatabase
        c = db.query("classesTable", null, null, null, null, null, null)
        if (c.moveToFirst()) {
            val numOfClass = c.getColumnIndex("numOfClass")
            do {
                list.add(c.getString(numOfClass))
            } while (c.moveToNext())
        }
        else list.add("NULL")
        c.close()
        dbHelper.close()
        list.sort()
        return list
    }

    fun addButClick() {
        finish()
        val intent = Intent(this, AddClassActivity::class.java)
        startActivity(intent)
    }

    fun classClick(numClass :String){
        finish()
        val intent = Intent(this, ClassInfoActivity::class.java)
        intent.putExtra("numClass",numClass)
        startActivity(intent)
    }

    public override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        menu?.add(0, MENU_DEL, 0, "Удалить");
        val bt=v as Button
        BTN_TXT= bt.text.toString()
    }

    public override fun onContextItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            MENU_DEL -> {
                deleteClass(BTN_TXT)
            }
        }

        return super.onContextItemSelected(item)
    }

    private fun deleteClass(numClass:String) {
        deleteChildrensFromClass(numClass)
        val db = dbHelper.writableDatabase
        val args = arrayOf<String>(numClass)
        db.delete("classesTable","numOfClass = ?", args)
        dbHelper.close()
        finish()
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun deleteChildrensFromClass(numClass: String){
        val db = dbHelper.writableDatabase
        val args = arrayOf<String>(numClass)
        db.delete("childrensTable","numOfClass = ?", args)
        dbHelper.close()
    }

}